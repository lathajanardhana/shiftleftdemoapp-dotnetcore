﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using my_tdd_application_api.Interfaces;
using my_tdd_application_api.Models;

namespace my_tdd_application_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RolesController : ControllerBase
    {

        private readonly ILogger<RolesController> _logger;
        private readonly IRoleService _roleService;
        public RolesController(ILogger<RolesController> logger, IRoleService roleService)
        {
            _logger = logger;
            _roleService = roleService;
        }

        [HttpGet]
        public IEnumerable<Roles> GetAllRole()
        {
            return _roleService.returnAllRoleList();
        }

        [HttpGet("{roleID}")]
        public Roles GetRoleByRoleId(int roleID)
        {
            return _roleService.returnRoleDetailsforRoleId(roleID);
        }

        [HttpPost]
        public string AddRole(Roles myRole)
        {
            var added = _roleService.AddRoleToList(myRole);
            if (added) { return "Role is added"; }
            else { return "Invalid Role to add"; }
        }
    }
}