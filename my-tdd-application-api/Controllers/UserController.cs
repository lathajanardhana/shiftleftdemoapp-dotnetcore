﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using my_tdd_application_api.Interfaces;
using my_tdd_application_api.Models;

namespace my_tdd_application_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;
        private readonly IUserService _userService;
        public UserController(ILogger<UserController> logger, IUserService userService)
        {
            _logger = logger;
            _userService = userService;
        }

        [HttpGet]
        public IEnumerable<User> GetAllUser()
        {
            return _userService.returnAllUserList();
        }

        [HttpGet("{userId}")]
        public User GetUserByUserId(int userId)
        {
            return _userService.returnUserDetailsforUserId(userId);
        }      

        [HttpPost]
        public string AddUser(User myUser)
        {
            bool added = _userService.AddUserToList(myUser);
            if (added) { return "User is added"; }
            else { return "Invalid User to add"; }
        }

        [HttpPatch("{userId}&&{roleId}")]
        public User UpdateUserRole(int userId, int roleId)
        {
            _userService.updateUserRole(userId, roleId);
            return _userService.returnUserDetailsforUserId(userId);
        }

        [HttpPatch("{userId}")]
        public User RefreshUserCourse(int userId)
        {
            _userService.updateUserCourse(userId);
            return _userService.returnUserDetailsforUserId(userId);
        }
		
		[HttpDelete("{userId}")]
        public void DeleteUser(int userId)
        {
           
        }

    }
}