﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using my_tdd_application_api.Interfaces;
using my_tdd_application_api.Models;

namespace my_tdd_application_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class Homepage : ControllerBase
    {
        private readonly ILogger<Homepage> _logger;
        private readonly IUserService _userService;
        public Homepage(ILogger<Homepage> logger, IUserService userService)
        {
            _logger = logger;
            _userService = userService;
        }

        [HttpGet]
        public IEnumerable<User> myHomepage()
        {
            return _userService.returnAllUserList();
        }
    }
}