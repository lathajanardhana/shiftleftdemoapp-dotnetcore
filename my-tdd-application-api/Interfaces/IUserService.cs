﻿using my_tdd_application_api.Models;
using System.Collections.Generic;

namespace my_tdd_application_api.Interfaces
{
    public interface IUserService
    {
        public List<User> returnAllUserList();

        public User returnUserDetailsforUserId(int userId);

        public bool AddUserToList(User myUser);

        public bool updateUserRole(int userId, int roleId);

        public bool updateUserCourse(int userId);

    }
}