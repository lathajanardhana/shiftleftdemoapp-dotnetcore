﻿using my_tdd_application_api.Interfaces;
using my_tdd_application_api.Models;
using System.Collections.Generic;

namespace my_tdd_application_api.Services
{
    public class RoleService : IRoleService
    {
        List<Roles> _rolesItems;

        //RoleService will also extend Data Layer which has all DB Queries
        public RoleService()
        {
            _rolesItems = new List<Roles>() {
            new Roles(){roleId=101,roleName="myFirstRoleName"},
            new Roles() { roleId = 102, roleName = "myTestingRoleName" }
        };
        }
        public List<Roles> returnAllRoleList()
        {
            //return _data.GetAllRole();
            return _rolesItems;
        }

        public Roles returnRoleDetailsforRoleId(int roleId)
        {
            //return _data.GetRole(roleId)
            return _rolesItems.Find(x => x.roleId.Equals(roleId));
        }

        public bool AddRoleToList(Roles myRole)
        {
            //_rolesItems = _data.GetAllRole();
            var invalidRole = false;
            for (int ri = 0; ri < _rolesItems.Count; ri++) { if (_rolesItems[ri].roleId == myRole.roleId) { invalidRole = true; } };
            if (invalidRole) { return false; }
            //_data.AddRole(myRole);
            else { _rolesItems.Add(myRole); return true; }
        }
    }
}