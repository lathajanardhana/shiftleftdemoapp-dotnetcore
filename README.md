**READ ME**

*Branches*
- The master branch has the code showing the concept of Shift Left in the QA phase for micro service architecture.
- The dev branch shows a traditional setup of CI jobs.

**Contract Testing**
- The Sample for Contract file creation for the application as provider can be seen under ContractTesting project
- The project also shows the implementation of Provider Test and Provider State Setup.
- Pact broker for contract management is not reflected in the code as it requires additional infrastructure
- The build and Unit Test Execution jobs artifacts the dll's and reports
- Actual sample jobs and reports can be seen for API Test and UI Test execution
- LnP, Deploy and Scans jobs have been set up as dummy to restrict code and data

**Application set up Steps:**
- Clone the repository
- Open Visual Studio and load the .sln file present in location: \my-tdd-application-api\my-tdd-application-api
- Build the application and click on IIS Express from Visual Studio
- The application should run on port 44366 by default. This can be changed in my-tdd-application-api\my-tdd-application-api\Properties\launchSettings.json if the port number needs to be changed
- The app should load the Homepage. Url: https://localhost:44366/Homepage and should show the below data

**Local Test Execution and Coverage:**
- Tests can be run from the Visual Studio UI
- To run the tests and get the coverage from command line we can follow the below steps
- Go to the root folder where Readme.md is present dev\my-tdd-application-api
- Run dotnet build ./my-tdd-application-api/my-tdd-application-api.sln
- Run dotnet test ./my-tdd-application-api-test/my-tdd-application-api-test.csproj --logger "trx;LogFileName=testResults.trx" /p:Exclude="[xunit*]*" /p:CollectCoverage=true /p:CoverletOutputFormat=\"opencover,lcov\" /p:CoverletOutput=../coverage/

**Note:** The above command will generate the test execution report in .trx format and a coverage report in .xml format in the mentioned locations.

*The API collection for the project is available under TestProjects/PostmanCollection*

**Application Details:**
The application has following API’s available:

*HomepageController*
- Homepage: Shows all user data<br>
Endpoint: https://localhost:portNo/Homepage [GET] 
	 
*UserController*
- GetAllUser: Shows list of all users in the system<br>
Endpoint: https://localhost:portNo/api/User [GET]

- GetUserByUserId: Gets data for specific user<br>
Endpoint: https://localhost:portNo/api/User/1001 [GET]

- AddUser: Adds the user if sent userId is not already existing in the system<br>
Endpoint: https://localhost:portNo/api/User [POST]

- UpdateUserRole: Updates user role if userId and roleId is present in the system<br>
Endpoint: https://localhost:portNo/api/User/1001&&102 [PATCH]

*RolesController*
- GetAllRole: Gets list of all roles in the system<br>
Endpoint: https://localhost:44366/api/Roles [GET]

- GetRoleByRoleId: Gets details of specific role for the given roleId<br>
Endpoint: https://localhost:44366/api/Roles/101 [GET]

- AddRole: Adds the role if sent roleId is not existing in the system<br>
Endpoint: https://localhost:44366/api/Roles [POST]